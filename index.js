// alert(`s24 discussion`)

// ES6 Updates

// Exponent Operator (**)
const firstNum = 8 ** 2;
console.log(firstNum);
// result: 64

// before ES6 update
const secondNum = Math.pow(8, 2);
console.log(secondNum);
// result: 64

/*
	Mini-Activity: 3 mins
		>> Using exponent operator get the cube of 5
		>> Store the result in a variable called getCube
		>> Print the result in the console browser
		>> Send your outputs in Hangouts
*/

let getCube = 5 ** 3
console.log(getCube)
// result: 125

// Template Literals

let name = "JP";

// before the update
let message = ('Hello ' + name + '! ' + 'Welcome to programming!');
console.log('Result from pre-template literals:');
console.log(message);

// ES6 updates
// this uses backticks(``)
message = (`Hello ${name}! Welcome to programming!`);
console.log(`Result with template literals:`)
console.log(message);

/*
	Mini-Activity:
		>> Create a sentence using the variables given below
		>> Send a screenshot output in Hangouts
*/

let string1 = `Zuitt`;
let string2 = `Coding`;
let string3 = `Bootcamp`;
let string4 = `teaches`;
let string5 = `Javascript`;
let string6 = `as a`;
let string7 = `programming language`;

console.log(`${string1} ${string2} ${string3} ${string4} ${string5} ${string6} ${string7}`)

const interestRate = .1;
const principal = 1000;

console.log(`The interest in your savings account is: ${principal * interestRate}`);
// result: 100

// Array Destructuring
/*
	Syntax:
		let / const = [variableNameA, variableB, ...] = arrayName
*/
const fullName = [`Juan`, `Dela`, `Cruz`];

// before update
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);

// ES6 updates
const [firstName, middleName, lastName] = fullName;

// assigning only middle name
// const [, middleName, ] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

/*
	Mini-Activity: 5 mins
		>> Destructure the given array and save each element in the following variables

			pointGuard1 - Curry
			pointGuard2 - Lillard
			pointGuard3 - Paul
			pointGuard4 - Irving

			>> Print the 4 variables in the console
			>> Send your output in Hangouts
*/
let pointGuards = ['Curry', 'Lillard', 'Paul', 'Irving'];

let [pointGuard1, pointGuard2, pointGuard3, pointGuard4] = pointGuards

console.log(pointGuard1);
console.log(pointGuard2);
console.log(pointGuard3);
console.log(pointGuard4);

// Object Destructure
/*
	Syntax:
		let / cons {propertyNameA, propertyNameB, ...} = objectName 
*/
/* before updates*/
const person = {
	givenName: 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz'
};

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It was nice meeting you.`);

// ES6 Updates

const {givenName, maidenName, familyName} = person;

// note that order does not matter in object destructuring
// const {familyName, givenName, maidenName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}. It's nice meeting you again.`);

/*
	Mini-Activity
		>> Given the object below, destructure each property
		>> Create a sentence variable with the following string message:

			My Pokemon is <pokemon1Name>, it is level < levelOfPokemon>. It is a <typeOfPokemon> type.

		>> Send your output in Hangouts
*/

let pokemon1 = {
	Name: "Charmander",
	Level: 11,
	Type: "Fire"
};

let {Name,Level, Type} = pokemon1;

console.log(`My Pokemon is ${Name}, it is level ${Level}. It is a ${Type} type`);

// destructuring in function parameter
function getFullName({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`)
};

getFullName(person);

// Arrow Function
/*
	Syntax:
		const variableName = (parameter) => {
			statement / code block
		}
*/

// before update

// function printFullName(firstName, middleName, lastName){

// 	console.log(`${firstName} ${middleName} ${lastName}`)

// };

// printFullName('John', 'D', 'Smith');

// ES6 Update

const printFullName = (firstName, middleName, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`);
};

printFullName('John', 'D', 'Smith');

const students = ['John', 'Jane', 'Juan'];

// forEach with traditional function
// students.forEach(function(){

// });

// Arrow Function
students.forEach(student => {
	console.log(`${student} is a student.`)
});
// implicit return
students.forEach(student => console.log(`${student} is a student.`));

// explicit return
// const add = (x, y) => {
// 	return x + y
// };

// let total = add(9,18);
// console.log(total);
// result: 27



// implicit return
const add = (x, y) => x + y

let total = add(27, 9);
console.log(total);
// result: 36

// Default function argument value

const greet = (name = 'User') => {
	return `Good day! ${name}.`
};

console.log(greet());

// traditional function

// function greet(name = 'User'){
// 	return `Good day! ${name;}`
// }

// Class-Based Object Blueprints
/*
	Syntax:

		class className {
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA
				this.objectPropertyB = objectPropertyB
			}
		}
*/

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
};

// Syntax:
	// let / const variableName = new className()

	// const myCar = new Car();

	let myCar = new Car ();
	console.log(myCar);
	// result: undefined

	myCar.brand = 'Ford';
	myCar.name = 'Ranger Raptor';
	myCar.year = 2021;

	console.log(myCar);
	const newCar = new Car("Toyota", "Vios", 2021);
	console.log(newCar);